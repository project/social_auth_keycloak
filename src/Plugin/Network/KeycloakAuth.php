<?php

declare(strict_types=1);

namespace Drupal\social_auth_keycloak\Plugin\Network;

use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Url;
use Drupal\social_api\SocialApiException;
use Drupal\social_auth\Plugin\Network\NetworkBase;
use Drupal\social_auth_keycloak\Settings\KeycloakAuthSettingsInterface;
use Stevenmaguire\OAuth2\Client\Provider\Keycloak;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines Social Auth Keycloak Network Plugin.
 *
 * @Network(
 *   id = "social_auth_keycloak",
 *   social_network = "Keycloak",
 *   type = "social_auth",
 *   handlers = {
 *     "settings": {
 *       "class": "Drupal\social_auth_keycloak\Settings\KeycloakAuthSettings",
 *       "config_id": "social_auth_keycloak.settings"
 *     },
 *   },
 * )
 */
final class KeycloakAuth extends NetworkBase {

  private ClientFactory $httpClientFactory;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->httpClientFactory = $container->get('http_client_factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function initSdk(): ?Keycloak {
    $class_name = Keycloak::class;
    if (!\class_exists($class_name)) {
      throw new SocialApiException(\sprintf('The Keycloak library for PHP League OAuth2 not found. Class: %s.', $class_name));
    }

    if (!$this->validateConfig()) {
      return NULL;
    }

    $settings = $this->settings;
    \assert($settings instanceof KeycloakAuthSettingsInterface);

    // All these settings are mandatory.
    $league_settings = [
      'authServerUrl' => $settings->getServerUrl(),
      'realm' => $settings->getRealm(),
      'clientId' => $settings->getClientId(),
      'clientSecret' => $settings->getClientSecret(),
      'redirectUri' => Url::fromRoute('social_auth_keycloak.callback')->setAbsolute()->toString(),
    ];

    if ($settings->hasEncryption()) {
      $league_settings['encryptionAlgorithm'] = $settings->getEncryptionAlgorithm();
      $league_settings['encryptionKey'] = $settings->getEncryptionKey();
    }

    // Proxy configuration data for outward proxy.
    $proxy_config = $this->httpClientFactory->fromOptions()->getConfig('proxy');
    if (\array_key_exists('https', $proxy_config)) {
      $league_settings['proxy'] = $proxy_config['https'];
    }
    elseif (\array_key_exists('proxy', $league_settings) && \array_key_exists('http', $proxy_config)) {
      $league_settings['proxy'] = $proxy_config['http'];
    }

    return new Keycloak($league_settings);
  }

  /**
   * Checks that module is configured.
   *
   * @return bool
   *   True if module is configured, false otherwise.
   */
  private function validateConfig(): bool {
    \assert($this->settings instanceof KeycloakAuthSettingsInterface);
    $callables = [
      [$this->settings, 'getServerUrl'],
      [$this->settings, 'getRealm'],
      [$this->settings, 'getClientId'],
      [$this->settings, 'getClientSecret'],
    ];
    foreach ($callables as $callable) {
      if ($callable() === NULL) {
        $this->loggerFactory
          ->get('social_auth_keycloak')
          ->error('Define %key in module settings.', ['%key' => substr(end($callable), 3)]);

        return FALSE;
      }
    }
    if ($this->settings->hasEncryption() && $this->settings->getEncryptionKey() === NULL) {
      $this->loggerFactory
        ->get('social_auth_keycloak')
        ->error('Define an encryption key in module settings.');

      return FALSE;
    }

    return TRUE;
  }

}
