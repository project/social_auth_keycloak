<?php

declare(strict_types=1);

namespace Drupal\social_auth_keycloak\Controller;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\Controller\OAuth2ControllerBase;
use Drupal\social_auth\SocialAuthDataHandler;
use Drupal\social_auth\User\UserAuthenticator;
use Drupal\social_auth_keycloak\Event\KeycloakEvent;
use Drupal\social_auth_keycloak\KeycloakAuthManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Manages requests to Keycloak API.
 */
final class KeycloakAuthController extends OAuth2ControllerBase {

  private $dispatcher;

  /**
   * Constructor for KeycloakAuthController.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\social_api\Plugin\NetworkManager $network_manager
   *   Used to get an instance of social_auth_keycloak network plugin.
   * @param \Drupal\social_auth\User\UserAuthenticator $user_authenticator
   *   Used to manage user authentication/registration.
   * @param \Drupal\social_auth_keycloak\KeycloakAuthManager $keycloak_manager
   *   Used to manage authentication methods.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Used to access GET parameters.
   * @param \Drupal\social_auth\SocialAuthDataHandler $data_handler
   *   The Social Auth data handler (used for session management).
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Used to handle metadata for redirection to authentication URL.
   */
  public function __construct(MessengerInterface $messenger,
                              NetworkManager $network_manager,
                              UserAuthenticator $user_authenticator,
                              KeycloakAuthManager $keycloak_manager,
                              RequestStack $request,
                              SocialAuthDataHandler $data_handler,
                              RendererInterface $renderer,
                              EventDispatcherInterface $dispatcher
  ) {
      $this->dispatcher = $dispatcher;
    parent::__construct(
      'Social Auth Keycloak', 'social_auth_keycloak',
      $messenger, $network_manager, $user_authenticator,
      $keycloak_manager, $request, $data_handler, $renderer);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('messenger'),
      $container->get('plugin.network.manager'),
      $container->get('social_auth.user_authenticator'),
      $container->get('social_auth_keycloak.manager'),
      $container->get('request_stack'),
      $container->get('social_auth.data_handler'),
      $container->get('renderer'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Callback function to login user.
   */
  public function callback(): Response {
    $request_query = $this->request->getCurrentRequest()->query;

    // Checks if authentication failed.
    if ($request_query->has('error')) {
      $this->messenger->addError($this->t('You could not be authenticated.'));

      $response = $this->userAuthenticator->dispatchAuthenticationError($request_query->get('error'));
      if ($response) {
        return $response;
      }

      return $this->redirect('user.login');
    }

    /** @var \Stevenmaguire\OAuth2\Client\Provider\KeycloakResourceOwner|null $resource_owner */
    $resource_owner = $this->processCallback();

    // If authentication was successful.
    if ($resource_owner !== NULL) {
      // Gets extra data.
      $data = $this->providerManager->getExtraDetails();
      $picture_url = $data['picture'] ?? NULL;
      $redirect = $this->userAuthenticator->authenticateUser($resource_owner->getName(), $resource_owner->getEmail(), $resource_owner->getId(), $this->providerManager->getAccessToken(), $picture_url, $data);
      $event = new KeycloakEvent($this->currentUser(), $resource_owner);
      $this->dispatcher->dispatch($event);
      return $redirect;
    }

    return $this->redirect('user.login');
  }

}
