<?php

declare(strict_types=1);

namespace Drupal\social_auth_keycloak\Settings;

use Drupal\social_auth\Settings\SettingsBase;

/**
 * Returns the client information.
 */
final class KeycloakAuthSettings extends SettingsBase implements KeycloakAuthSettingsInterface {

  private ?string $serverUrl = NULL;
  private ?string $realm = NULL;
  private ?string $clientId = NULL;
  private ?string $clientSecret = NULL;
  private ?string $encryptionAlgorithm = NULL;
  private ?string $encryptionKey = NULL;

  /**
   * {@inheritdoc}
   */
  public function getServerUrl(): ?string {
    if (!$this->serverUrl) {
      $this->serverUrl = $this->config->get('server_url');
    }
    return $this->serverUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function getRealm(): ?string {
    if (!$this->realm) {
      $this->realm = $this->config->get('realm');
    }
    return $this->realm;
  }

  /**
   * {@inheritdoc}
   */
  public function getClientId(): ?string {
    if (!$this->clientId) {
      $this->clientId = $this->config->get('client_id');
    }
    return $this->clientId;
  }

  /**
   * {@inheritdoc}
   */
  public function getClientSecret(): ?string {
    if (!$this->clientSecret) {
      $this->clientSecret = $this->config->get('client_secret');
    }
    return $this->clientSecret;
  }

  /**
   * {@inheritdoc}
   */
  public function hasEncryption(): bool {
    return (bool) $this->config->get('encryption_algorithm');
  }

  /**
   * {@inheritdoc}
   */
  public function getEncryptionAlgorithm(): ?string {
    if (!$this->encryptionAlgorithm) {
      $this->encryptionAlgorithm = $this->config->get('encryption_algorithm');
    }
    return $this->encryptionAlgorithm;
  }

  /**
   * {@inheritdoc}
   */
  public function getEncryptionKey(): ?string {
    if (!$this->encryptionKey) {
      if ($this->config->get('encryption_key') !== NULL) {
        $this->encryptionKey = $this->config->get('encryption_key');
      }
      else {
        $this->encryptionKey = \file_get_contents($this->config->get('encryption_key_path'));
      }
    }
    return $this->encryptionKey;
  }

}
