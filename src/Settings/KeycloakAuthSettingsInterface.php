<?php

declare(strict_types=1);

namespace Drupal\social_auth_keycloak\Settings;

/**
 * Defines an interface for Social Auth Keycloak settings.
 */
interface KeycloakAuthSettingsInterface {

  /**
   * Gets the server URL.
   *
   * @return string|null
   *   The server URL.
   */
  public function getServerUrl(): ?string;

  /**
   * Gets the realm.
   *
   * @return string|null
   *   The realm.
   */
  public function getRealm(): ?string;

  /**
   * Gets the client ID.
   *
   * @return string|null
   *   The client ID.
   */
  public function getClientId(): ?string;

  /**
   * Gets the client secret.
   *
   * @return string|null
   *   The client secret.
   */
  public function getClientSecret(): ?string;

  /**
   * Is encryption enabled.
   *
   * @return bool
   *   True if encryption enabled, false otherwise.
   */
  public function hasEncryption(): bool;

  /**
   * Gets the encryption algorithm.
   *
   * @return string|null
   *   The encryption algorithm.
   */
  public function getEncryptionAlgorithm(): ?string;

  /**
   * Gets the encryption key.
   *
   * @return string|null
   *   The encryption key.
   */
  public function getEncryptionKey(): ?string;

}
