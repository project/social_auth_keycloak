<?php

declare(strict_types=1);

namespace Drupal\social_auth_keycloak\Event;

use Drupal\Core\Session\AccountInterface;
use Stevenmaguire\OAuth2\Client\Provider\KeycloakResourceOwner;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Provides the event class for Keycloak events.
 */
final class KeycloakEvent extends Event {

  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  private AccountInterface $drupalAccount;

  /**
   * @var \Stevenmaguire\OAuth2\Client\Provider\KeycloakResourceOwner
   */
  private KeycloakResourceOwner $resourceOwner;

  /**
   * Constructs a KeycloakEvent.
   *
   * @param \Drupal\Core\Session\AccountInterface $drupal_user
   *   The Drupal account.
   * @param \Stevenmaguire\OAuth2\Client\Provider\KeycloakResourceOwner $resource_owner
   *   The keycloak resource owner.
   */
  public function __construct(
    AccountInterface $drupal_user,
    KeycloakResourceOwner $resource_owner
  ) {
    $this->drupalAccount = $drupal_user;
    $this->resourceOwner = $resource_owner;
  }

  public function getDrupalAccount(): AccountInterface {
    return $this->drupalAccount;
  }

  public function getResourceOwner(): ?KeycloakResourceOwner {
    return $this->resourceOwner;
  }

}
