<?php

declare(strict_types=1);

namespace Drupal\social_auth_keycloak\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\social_auth\Form\SocialAuthSettingsForm;

/**
 * Settings form for Social Auth Keycloak.
 */
final class KeycloakAuthSettingsForm extends SocialAuthSettingsForm {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return \array_merge(['social_auth_keycloak.settings'], parent::getEditableConfigNames());
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'social_auth_keycloak_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('social_auth_keycloak.settings');

    $form['keycloak_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Keycloak Client settings'),
      '#open' => TRUE,
      '#description' => $this->t('You need to first create a Keycloak Client at Keycloak => Administration Console => Clients'),
    ];
    $form['keycloak_settings']['server_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Server URL'),
      '#default_value' => $config->get('server_url'),
      '#description' => $this->t('The keycloak server url i.e. http://localhost:8080/auth'),
    ];
    $form['keycloak_settings']['realm'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Realm'),
      '#default_value' => $config->get('realm'),
      '#description' => $this->t('Keycloak realm i.e. master'),
    ];
    $form['keycloak_settings']['client_id'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('client_id'),
      '#description' => $this->t('Copy the Client ID here'),
    ];
    $form['keycloak_settings']['client_secret'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Client Secret'),
      '#default_value' => $config->get('client_secret'),
      '#description' => $this->t('Copy the Client Secret here'),
    ];
    $form['keycloak_settings']['authorized_redirect_url'] = [
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#title' => $this->t('Authorized redirect URIs'),
      '#description' => $this->t('Copy this value to <em>Valid Redirect URIs</em> field of the client.'),
      '#default_value' => Url::fromRoute('social_auth_keycloak.callback')->setAbsolute()->toString(),
    ];

    $form['keycloak_settings']['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced settings'),
      '#open' => FALSE,
    ];
    $form['keycloak_settings']['advanced']['scopes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Scopes for API call'),
      '#default_value' => $config->get('scopes'),
      '#description' => $this->t('Define any additional scopes to be requested, separated by a space (" ").'),
    ];
    $form['keycloak_settings']['advanced']['encryption_algorithm'] = [
      '#type' => 'textfield',
      '#title' => $this->t('(optional) Encryption algorithm'),
      '#default_value' => $config->get('encryption_algorithm'),
      '#description' => $this->t('Review client access token signature algorithm configuration in Keycloak for the correct algorithm i.e. RS256. Only provide a key or a path to a key. Not both.'),
    ];
    $form['keycloak_settings']['advanced']['encryption_key_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('(optional) Encryption key path'),
      '#description' => $this->t('Path to public key or certificate file i.e. ../key.pem.'),
      '#default_value' => $config->get('encryption_key_path'),
    ];
    $form['keycloak_settings']['advanced']['encryption_key'] = [
      '#type' => 'textarea',
      '#title' => $this->t('(optional) Encryption key/certificate'),
      '#description' => $this->t('Ex. -----BEGIN PUBLIC KEY-----\n....\n-----END PUBLIC KEY----- or -----BEGIN CERTIFICATE-----\n....\n-----END CERTIFICATE----- Only provide a key or a path to a key. Not both.'),
      '#default_value' => $config->get('encryption_key'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();

    $this->config('social_auth_keycloak.settings')
      ->set('server_url', $values['server_url'])
      ->set('realm', $values['realm'])
      ->set('client_id', $values['client_id'])
      ->set('client_secret', $values['client_secret'])
      ->set('scopes', $values['scopes'])
      ->set('encryption_algorithm', $values['encryption_algorithm'])
      ->set('encryption_key_path', $values['encryption_key_path'])
      ->set('encryption_key', $values['encryption_key'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
