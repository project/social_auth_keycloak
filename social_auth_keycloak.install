<?php

/**
 * @file
 * Install and uninstall functions for the Social Auth Keycloak module.
 */

declare(strict_types=1);

use Drupal\social_api\Utility\SocialApiImplementerInstaller;
use Drupal\social_auth\Controller\SocialAuthController;

/**
 * Implements hook_requirements().
 */
function social_auth_keycloak_requirements(string $phase): array {
  $requirements = [];

  // Social API should be installed at this point in order to check library.
  \Drupal::service('module_installer')->install(['social_api']);

  // When the site builder wants to enable this module.
  if ($phase === 'install') {
    // We check that the required library was downloaded beforehand.
    $requirements = SocialApiImplementerInstaller::checkLibrary('social_auth_keycloak', 'Social Auth Keycloak', 'stevenmaguire/oauth2-keycloak', 2.0, 3.0);
  }

  return $requirements;
}

/**
 * Implements hook_install().
 *
 * Method setLoginButtonSettings() sets up the settings for the login button.
 *
 * This button is used for the 'Social Auth Login Block' to add a link to
 * allow users login with the implementing service.
 *
 * The first argument must be the name of the module you are building.
 * The second argument is the route the link will point to.
 * The third one is the path (from the root of the module) to the image which
 * will be used as a link in the 'Social Auth Login Block'.
 */
function social_auth_keycloak_install(): void {
  SocialAuthController::setLoginButtonSettings('social_auth_keycloak', 'social_auth_keycloak.redirect_to_provider', 'img/keycloak_icon_256px.svg');
}

/**
 * Implements hook_uninstall().
 *
 * Method deleteLoginButtonSettings() deletes the settings for the login button.
 *
 * When the module is uninstalled, you need to remove the login button for the
 * implementer from the 'Social Auth Login Block'.
 *
 * The argument passed to the method must be the name of the module you are
 * building.
 */
function social_auth_keycloak_uninstall(): void {
  SocialAuthController::deleteLoginButtonSettings('social_auth_keycloak');
}
