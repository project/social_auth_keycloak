<?php

declare(strict_types=1);

namespace Drupal\Tests\social_auth_keycloak\Functional;

use Drupal\Tests\social_auth\Functional\SocialAuthTestBase;

/**
 * Test that path to authentication route exists in Social Auth Login block.
 *
 * @group social_auth
 *
 * @ingroup social_auth_keycloak
 */
final class SocialAuthKeycloakLoginBlockTest extends SocialAuthTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['block', 'social_auth_keycloak'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->provider = 'keycloak';
  }

  /**
   * Test that the path is included in the login block.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testLinkExistsInBlock(): void {
    $this->checkLinkToProviderExists();
  }

}
