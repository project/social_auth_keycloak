<?php

declare(strict_types=1);

namespace Drupal\Tests\social_auth_keycloak\Functional;

use Drupal\Tests\social_auth\Functional\SocialAuthTestBase;

/**
 * Test Social Auth Keycloak settings form.
 *
 * @group social_auth
 *
 * @ingroup social_auth_keycloak
 */
final class SocialAuthKeycloakSettingsFormTest extends SocialAuthTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['social_auth_keycloak'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $this->module = 'social_auth_keycloak';
    $this->provider = 'keycloak';

    parent::setUp();
  }

  /**
   * Test if implementer is shown in the integration list.
   */
  public function testIsAvailableInIntegrationList(): void {
    $this->checkIsAvailableInIntegrationList();
  }

  /**
   * Test if permissions are set correctly for settings page.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testPermissionForSettingsPage(): void {
    $this->checkPermissionForSettingsPage();
  }

  /**
   * Test settings form submission.
   */
  public function testSettingsFormSubmission(): void {
    $this->edit = [
      'server_url' => $this->randomString(),
      'realm' => $this->randomString(),
      'client_id' => $this->randomString(10),
      'client_secret' => $this->randomString(10),
    ];

    $this->checkSettingsFormSubmission();
  }

}
